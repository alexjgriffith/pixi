var utils = {
    clamp: function(x, min, max){
        return Math.min(Math.max(x,min), max);
    }
}

function main (){
    console.log(colour);
    surface.main();
    colour.main();
}

surface = function(utils){
    var mouse = {down:false,click:{x:[], y:[]}};
    var game_state = {};
    var ctx;
    function main (){
        var canvas = document.getElementById("surface");
        ctx = canvas.getContext("2d");
        game_state = init_game_state(Layer(64,["null","#FFFFFF","#000000"]));
        canvas.height = 400;
        canvas.width = 400;
        canvas.addEventListener("mousedown", click_callback);
        canvas.addEventListener("mousemove", move_callback);
        canvas.addEventListener("mouseup", up_callback);
        canvas.addEventListener("mouseout", up_callback);
        window.requestAnimationFrame(gameloop);
    }
    
    function init_game_state (layer){
        var game_state={current_layer:0,layers:[layer]};
        return game_state;
    }
    function move_callback(event){
        if(mouse.down){
            var rect = ctx.canvas.getBoundingClientRect();
            // add interpolation
            mouse.click.x.push(event.clientX-rect.left);
            mouse.click.y.push(event.clientY-rect.top);
        }
    }
    function click_callback (event){
        mouse.down=true;
        var rect = ctx.canvas.getBoundingClientRect();
        mouse.click.x.push(event.clientX-rect.left);
        mouse.click.y.push(event.clientY-rect.top);

    }
    function up_callback(){
        mouse.down=false;
    }
    function gameloop (){
        game_state = update();
        drawColours();
        // drawBorder();
        // drawGrid();
        window.requestAnimationFrame(gameloop);
    }

    function update(){
        var x,y;
        var layer = game_state.layers[game_state.current_layer];        
        // console.log(layer.data);
        if(mouse.click.x.length > 0)
        {
            x = Math.floor(mouse.click.x[0]/ctx.canvas.width * layer.data.width);
            y = Math.floor(mouse.click.y[0]/ctx.canvas.width * layer.data.width);
            // console.log([mouse.click.x[0],layer.data.width,
            //              clamp(x,0,layer.data.width),
            //              clamp(y,0,layer.data.width)])
            layer.add(utils.clamp(x,0,layer.data.width),
                      utils.clamp(y,0,layer.data.width))
            // console.log(game_state);
            mouse.click.x=[]
            mouse.click.y=[]        
        }
        
        return game_state;
    };

    function drawBorder (){
        var h = ctx.canvas.height;
        var w = ctx.canvas.width;
        ctx.strokeStyle = "#000000";
        ctx.strokeRect(0,0,w,h);
        ctx.stroke();
    }
    
    function drawColours (){
        var i,j;
        var x,y;
        var layer = game_state.layers[game_state.current_layer];
        var step = ctx.canvas.width / (layer.data.width );
        var colour;
        for(i=0;i<(layer.data.width ); i++){
            for(j=0;j<(layer.data.width); j++){
                colour = layer.data.pixels[i][j];
                if (colour != "null")
                {
                    x=utils.clamp(step*i,0,step*layer.data.width);
                    y=utils.clamp(step*j,0,step*layer.data.width);
                    ctx.fillStyle = layer.data.colours[colour];                
                    ctx.fillRect(x,y,step,step);
                }
            }
        }    
    }

    function drawGrid (){
        var i,j;
        var layer = game_state.layers[game_state.current_layer];
        var step = ctx.canvas.width / layer.data.width ;
        for(i=1;i<(layer.data.width ); i++){
            for(j=1;j<(layer.data.width ); j++){
                ctx.fillStyle = "#000000";
                ctx.fillRect(i*step-1,j*step-1,2,2);
            }            
        }        
    }
    return {main: main,
            game_state:game_state};
}(utils)

colour = function (utils){
    function main (){
        var canvas = document.getElementById("colour");
        ctx = canvas.getContext("2d");
        // game_state = init_game_state(Layer(16,["null","#FFFFFF","#000000"]));
        canvas.height = 100;
        canvas.width = 400;
        // canvas.addEventListener("mousedown", click_callback);
        window.requestAnimationFrame(gameloop);
    }
    function gameloop (){
        draw ();
        window.requestAnimationFrame(gameloop);
    }

    function draw () {
        ctx.fillStyle="red";
        ctx.fillRect(10,10,40,80)
        for(i=0;i<6;i++){
            ctx.fillRect(10+50*(i + 1),10,40,35)
            ctx.fillRect(10+50*(i + 1),55,40,35)
        }
    }

    return {main:main};
}(utils)

function Layer (width,colours){
    function initialize_pixels (x,value){
        var pixels=[];
        for (i=0; i<x; i++){
            pixels.push([]);
            for (j=0; j<x; j++){
                pixels[i][j]=value;
            }
        }
        return pixels;
    }
    function com_json (command, x, y, newarg, oldarg){
        return {command:command,
                x:x,
                y:y,
                newarg:newarg,
                oldarg:oldarg,
               };
    }
    function add (x,y){
        var message = com_json("swap",x,y,
                               this.data.current_colour,
                               this.data.pixels_id[x][y]);
        this.data.pixels_id[x][y] = this.data.current_colour;
        this.data.pixels[x][y] = this.data.colours[this.data.current_colour];
        // console.log(message);
        return  message;
    }
    function erase (x,y){
        var message = com_json("swap",x,y,0,this.data.pixels_id[x][y]);
        this.data.pixels_id[x][y] = 0;
        this.data.pixels[x][y]="null";
        // console.log(message);
        return message;
    }
    return {data:{width:width,
                  height:width,
                  current_colour:1,
                  colours:colours,
                  pixels:initialize_pixels(width,"null"),
                  pixels_id:initialize_pixels(width,0),
                  commands:[]},
            add:add,
            erase:erase
           };
}
